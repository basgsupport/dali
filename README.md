﻿---

## About

This sample project is about how to use a DALI device. On top of that it also contain an example on how to use in OOP maner

---


## Pre-requisite

None

```
The project is an example by itself
```


---

## How to use

Open up the project and study how to use it yourself

---

## Help

Contact support@beckhoff.com.sg


